import React, {Component} from 'react';
import './App.css';
import {Button, FormGroup, Table} from 'reactstrap';
import Restaurant from "./components/Restaurant";
import RestaurantForm from "./components/RestaurantForm";
import Pagination from "./components/Pagination";
import {ControlLabel, FormControl} from "react-bootstrap";

class App extends Component {

    constructor() {
        super();
        this.state = {
            restaurants: [],
            page: 1,
            pageSize: 10,
            count: 0,
            search: null,
            showForm: false,
            showEditForm: false,
            restaurantToEdit: null
        }
    }

    componentDidMount() {
        this.fetchData();
    }

    render() {
        let restaurants = this.state.restaurants.map((el, index) => {
            return (
                <Restaurant id={el._id}
                            key={index}
                            cuisine={el.cuisine}
                            name={el.name}
                            fetchData={this.fetchData.bind(this)}
                            updateRestaurantToEdit={this.updateRestaurantToEdit.bind(this)}
                />
            )
        });

        return (
            <div className="App">
                <form>
                    <FormGroup>
                        <ControlLabel>Elements per page</ControlLabel>
                        <FormControl componentClass="select" name="pageSize" onChange={this.handleChange.bind(this)} defaultValue={this.state.pageSize}>
                            <option value="5">5</option>
                            <option value="10">10</option>
                            <option value="15">15</option>
                        </FormControl>
                        <FormControl
                            type="text"
                            name="search"
                            value={this.state.value}
                            placeholder="Chercher par nom"
                            onChange={this.handleChange.bind(this)}
                        />
                    </FormGroup>
                </form>

                <Table bordered>
                    <thead>
                    <tr>
                        <th>Nom</th>
                        <th>Cuisine</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        {restaurants}
                    </tbody>
                </Table>

                <br/>
                <Pagination updateCurrentPage={this.updateCurrentPage.bind(this)} currentPage={this.state.page} count={this.state.count} pageSize={this.state.pageSize} fetchData={this.fetchData.bind(this)}/>
                <div>
                    <Button color={this.state.showForm ? "danger" : "success"} onClick={() => this.onAddRestaurant()}>{this.state.showForm ? "-" : "+" }</Button>
                </div>
                <div>
                    { this.state.showEditForm && this.state.restaurantToEdit != null ? <RestaurantForm edit={true} onEditRestaurant={this.onEditRestaurant.bind(this)} restaurant={this.state.restaurantToEdit} fetchData={this.fetchData.bind(this)}/> : null }
                </div>
                <div>
                    { this.state.showForm ? <RestaurantForm onAddRestaurant={this.onAddRestaurant.bind(this)} fetchData={this.fetchData.bind(this)}/> : null }
                </div>
            </div>
        );
    }

    handleChange(e) {
        let state = this.state;
        if (e.target.name === "pageSize") {
            var newPageSize = e.target.value;
            var newPage = Math.ceil(((this.state.page - 1) * this.state.pageSize + 1) / newPageSize);
            state["page"] = newPage;
        }
        state[e.target.name] = e.target.value;

        this.setState({state});
        this.fetchData();
    }

    fetchData(arg) {
        var url = this.buildUrl(arg);
        console.log("Calling URL: " + url);
        fetch(url)
            .then(results => {
                return results.json();
            })
            .then(data => {
                this.setState({restaurants: data.data, showForm: false, count: data.count});
            })
    }

    buildUrl(params) {
        var url = "http://localhost:8080/api/restaurants";
        url = url.concat("?page=");
        if(params !== undefined && params.page !== undefined) {
            url = url.concat(params.page)
        } else {
            url = url.concat(this.state.page);
        }

        url = url.concat("&pagesize=");
        if(params !== undefined && params.pageSize !== undefined) {
            url = url.concat(params.pageSize)
        } else {
            url = url.concat(this.state.pageSize);
        }

        if(this.state.search != null && this.state.search !== '') {
            url = url.concat("&name=");
            url = url.concat(this.state.search);
        }
        return url;
    }

    onAddRestaurant() {
        this.setState({showForm: !this.state.showForm});
        if (this.state.showEditForm) {
            this.setState({showEditForm: false});
        }
    }

    onEditRestaurant() {
        this.setState({showEditForm: !this.state.showEditForm});
        this.setState({showForm: false});
    }

    updateCurrentPage(newPage) {
        this.setState({page: newPage});
    }

    updateRestaurantToEdit(restaurant) {
        this.setState({showEditForm: !this.state.showEditForm});
        this.setState({restaurantToEdit: restaurant});
        if (this.state.showForm) {
            this.setState({showForm: false});
        }
    }

}

export default App;
