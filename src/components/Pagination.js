import React, { Component } from 'react';
import PageTile from "./PageTile";

class Pagination extends Component {

    constructor(props) {
        super(props);

        this.state = {
            currentPage: props.currentPage ? props.currentPage : 1,
            count: props.count ? props.count : 10,
            pageSize: props.pageSize ? props.pageSize : 10,
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            currentPage: nextProps.currentPage,
            count: nextProps.count,
            pageSize: nextProps.pageSize
        });
    }

    componentDidMount() {
        this.getCount();
    }

    render() {
        return (
            <div>
                <table>
                    <tbody><tr>
                        {this.renderPreviousPages()}
                        <td><PageTile no={this.state.currentPage} updateCurrentPage={this.updateCurrentPage.bind(this)} fetchData={this.props.fetchData.bind(this)}/></td>
                        {this.renderNextPages()}
                    </tr></tbody>
                </table>
                <br/>
            </div>
        );
    }

    renderPreviousPages() {
        let pages = [];

        let i = this.state.currentPage - 1, counter = 0;
        while(i > 0 && counter < 1) {
            pages.push(<td key={i}><PageTile no={i} updateCurrentPage={this.updateCurrentPage.bind(this)} fetchData={this.props.fetchData.bind(this)}/></td>);

            i--; counter++;
        }
        if (i !== 0) {
            pages.push(<td key="L0">...............</td>);
            pages.push(<td key="first"><PageTile no={1} updateCurrentPage={this.updateCurrentPage.bind(this)} fetchData={this.props.fetchData.bind(this)}/></td>);
        }

        return pages.reverse();
    }

    renderNextPages() {
        let pages = [];
        let pagesNumber = Math.ceil(this.state.count/this.state.pageSize)

        let i = this.state.currentPage + 1, counter = 0;
        while(i <= pagesNumber && counter < 1) {
            pages.push(<td key={i}><PageTile no={i} updateCurrentPage={this.updateCurrentPage.bind(this)} fetchData={this.props.fetchData.bind(this)}/></td>);

            i++; counter++;
        }
        if (i !== pagesNumber + 1) {
            pages.push(<td key="R0">...............</td>);
            pages.push(<td key="last"><PageTile no={pagesNumber} updateCurrentPage={this.updateCurrentPage.bind(this)} fetchData={this.props.fetchData.bind(this)}/></td>);
        }

        return pages;
    }

    getCount() {
        fetch("http://localhost:8080/api/restaurants/count")
            .then(results => {
                return results.json();
            })
            .then(data => {
                this.setState({count: data.data});
            })
    }

    updateCurrentPage(newPage) {
        this.setState({currentPage: newPage});
        this.props.updateCurrentPage(newPage);
    }

}

export default Pagination;
