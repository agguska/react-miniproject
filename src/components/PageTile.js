import React, { Component } from 'react';
import {Button} from "reactstrap";

class PageTile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            no: props.no ? props.no : 1
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            no: nextProps.no
        });
    }

    render() {
        return (
            <Button color="info" onClick={() => this.onLoadPage(this.state.no)}>{this.state.no}</Button>
        )
    }

    onLoadPage(page) {
        this.setState({currentPage: page});
        this.props.fetchData({page: page});
        this.props.updateCurrentPage(page);
    }
}

export default PageTile;