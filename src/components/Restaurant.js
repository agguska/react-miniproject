import React, { Component } from 'react';
import {Button} from "reactstrap";

class Restaurant extends Component {

    constructor(props) {
        super(props);

        this.state = {
            restaurant: {
                id: props.id,
                name: props.name,
                cuisine: props.cuisine
            }
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            restaurant: {
                id: nextProps.id,
                name: nextProps.name,
                cuisine: nextProps.cuisine
             }
        });
    }

    render() {
        return (
            <tr>
                <th>{this.state.restaurant.name}</th>
                <th>{this.state.restaurant.cuisine}</th>
                <th>
                    <Button color="danger" onClick={() => this.removeRestaurant(this.state.restaurant.id)}>X</Button>
                    <Button color="warning" onClick={() => this.editRestaurant(this.state.restaurant.id)}>E</Button>
                </th>
            </tr>
        );
    }

    removeRestaurant(restaurantId) {

        var endpoint = "http://localhost:8080/api/restaurants/" + restaurantId;
        fetch(endpoint, {
            method: 'DELETE',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(this.state.restaurant)
        }).then((response) => response.json())
            .then((responseJson) => {
                //show success message
                console.log(responseJson);
                this.props.fetchData();
            })
            .catch((error) => {
                console.error(error);
                //show Message Error
            });
    }

    editRestaurant() {
        this.props.updateRestaurantToEdit(this.state.restaurant);
    }

}

export default Restaurant;
