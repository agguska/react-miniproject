import React, { Component } from 'react';
import {Button, FormGroup} from "reactstrap";
import {ControlLabel, FormControl} from "react-bootstrap";

class RestaurantForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            restaurant: {
                id: props.restaurant && props.restaurant.id ? props.restaurant.id : null,
                name: props.restaurant && props.restaurant.name ? props.restaurant.name : '',
                cuisine: props.restaurant && props.restaurant.cuisine ? props.restaurant.cuisine : ''
            },
            edit: props.edit ? props.edit : false
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            restaurant: {
                id: nextProps.id ? nextProps.id : null,
                name: nextProps.name ? nextProps.name : '',
                cuisine: nextProps.cuisine ? nextProps.cuisine : ''
            },
            edit: nextProps.edit ? nextProps.edit : false
        });
    }

    handleChange(e) {
        let restaurant = this.state.restaurant;
        restaurant[e.target.name] = e.target.value;
        this.setState({restaurant});
    }

    render() {
        return (
            <form>
                <FormGroup>
                    <ControlLabel htmlFor="name">Name</ControlLabel>
                    <FormControl required id="name" name="name" type="text" value={this.state.restaurant.name} onChange={this.handleChange.bind(this)} placeholder="Nom" /><br /><br />
                    <ControlLabel htmlFor="cuisine">Cuisine</ControlLabel>
                    <FormControl required id="cuisine" name="cuisine" type="text" value={this.state.restaurant.cuisine} onChange={this.handleChange.bind(this)} placeholder="Cuisine" /><br /><br />
                    { this.state.edit ?
                        <Button color="success" onClick={() => this.onEditRestaurant()}>Edit restaurant</Button>
                        : <Button color="success" onClick={() => this.onAddRestaurant()}>Add restaurant</Button>
                    }
                </FormGroup>
            </form>
        );
    }

    onAddRestaurant() {
        var endpoint = "http://localhost:8080/api/restaurants";

        var formData = new FormData();
        formData.append("nom", this.state.restaurant.name);
        formData.append("cuisine", this.state.restaurant.cuisine);

        fetch(endpoint, {
            method: 'POST',
            body: formData
        }).then((response) => response.json())
            .then((responseJson) => {
                //show success message
                console.log(responseJson);
                this.props.fetchData();
                this.props.onAddRestaurant();
            })
            .catch((error) => {
                console.error(error);
                //show Message Error
            });
    }

    onEditRestaurant() {
        var endpoint = "http://localhost:8080/api/restaurants/" + this.state.restaurant.id;

        var formData = new FormData();
        formData.append("_id", this.state.restaurant.id);
        formData.append("nom", this.state.restaurant.name);
        formData.append("cuisine", this.state.restaurant.cuisine);

        fetch(endpoint, {
            method: 'PUT',
            body: formData
        }).then((response) => response.json())
            .then((responseJson) => {
                //show success message
                console.log(responseJson);
                this.props.fetchData();
                this.props.onEditRestaurant();
            })
            .catch((error) => {
                console.error(error);
                //show Message Error
            });
    }
}
export default RestaurantForm;